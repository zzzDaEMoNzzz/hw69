import React, {Component} from 'react';
import {connect} from "react-redux";
import {getProducts} from "../../store/actions/products";
import {addToCart} from "../../store/actions/cart";
import Product from "../../components/Product/Product";
import './Products.css';
import Spinner from "../../components/UI/Spinner/Spinner";

class Products extends Component {
  componentDidMount() {
    this.props.getProducts();
  }

  render() {
    const products = this.props.products.map(product => (
      <Product
        key={product.id}
        name={product.name}
        price={product.price}
        image={product.image}
        handler={() => this.props.addToCart(product.id)}
      />
    ));

    if (this.props.loading) return <div><Spinner/></div>;

    return (
      <div className="Products">
        {products}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.pr.products,
  loading: state.pr.loading
});

const mapDispatchToProps = dispatch => ({
  getProducts: () => dispatch(getProducts()),
  addToCart: productID => dispatch(addToCart(productID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);