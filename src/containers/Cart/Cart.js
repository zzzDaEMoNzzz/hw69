import React, {Component} from 'react';
import {connect} from "react-redux";
import './Cart.css';
import OrderItem from "../../components/OrderItem/OrderItem";
import {removeFromCart} from "../../store/actions/cart";
import {checkoutShow} from "../../store/actions/checkout";

const COST_OF_DELIVERY = 150;

class Cart extends Component {
  render() {
    const order = Object.keys(this.props.cart).reduce((array, id) => {
      if (this.props.cart[id] > 0) {
        array.push(
          <OrderItem
            key={this.props.products[id].id}
            name={this.props.products[id].name}
            price={this.props.products[id].price}
            count={this.props.cart[id]}
            handler={() => this.props.removeFromCart(id)}
          />
        );
      }
      return array;
    }, []);

    const orderPrice = Object.keys(this.props.cart).reduce((num, id) => {
      if (this.props.cart[id] > 0) {
        num += this.props.cart[id] * this.props.products[id].price;
      }
      return num;
    }, 0);

    return (
      <div className="Cart">
        <h3>Корзина</h3>
        <div className="Cart-order">
          {order}
        </div>
        <div className="Cart-price">
          <p><span>Доставка</span> {COST_OF_DELIVERY}</p>
          <p><span>Итого</span> {COST_OF_DELIVERY + orderPrice}</p>
        </div>
        <button
          onClick={this.props.checkoutShow}
          disabled={order.length === 0}
          style={order.length === 0 ? {background: '#fff'} : null}
        >Оформить заказ</button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cart: state.cart.products,
  products: state.pr.products
});

const mapDispatchToProps = dispatch => ({
  removeFromCart: productID => dispatch(removeFromCart(productID)),
  checkoutShow: () => dispatch(checkoutShow())
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart)