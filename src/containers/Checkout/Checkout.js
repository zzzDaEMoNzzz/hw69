import React, {Component} from 'react';
import './Checkout.css';
import {connect} from "react-redux";
import {createOrder} from "../../store/actions/checkout";
import Spinner from "../../components/UI/Spinner/Spinner";

class Checkout extends Component {
  state = {
    name: '',
    address: '',
    telephone: ''
  };

  inputOnChange = (event, stateTarget) => {
    this.setState({
      [stateTarget]: event.target.value
    });
  };

  render() {
    const productsList = Object.keys(this.props.cart).reduce((array, id) => {
      if (this.props.cart[id] > 0) {
        array.push({
          name: this.props.products[id].name,
          count: this.props.cart[id]
        });
      }
      return array;
    }, []);

    const order = {
      productsList,
      contacts: {
        name: this.state.name,
        address: this.state.address,
        telephone: this.state.telephone,
      }
    };

    if (this.props.loading) return <Spinner/>;

    return (
      <form className="Checkout" onSubmit={event => this.props.createOrder(event, order)}>
        <input
          type="text"
          placeholder="Имя"
          name="name"
          required
          value={this.state.name}
          onChange={event => this.inputOnChange(event, 'name')}
        />
        <input
          type="text"
          placeholder="Адрес"
          name="address"
          required
          value={this.state.address}
          onChange={event => this.inputOnChange(event, 'address')}
        />
        <input
          type="text"
          placeholder="Телефон"
          name="telephone"
          required
          value={this.state.telephone}
          onChange={event => this.inputOnChange(event, 'telephone')}
        />
        <button>Отправить заказ</button>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  cart: state.cart.products,
  products: state.pr.products,
  loading: state.checkout.loading
});

const mapDispatchToProps = dispatch => ({
  createOrder: (event, order) => dispatch(createOrder(event, order))
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout)