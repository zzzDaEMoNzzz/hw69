import React from 'react';
import './Product.css';

const Product = ({name, price, image, handler}) => {
  return (
    <div className="Product">
      <img src={image} alt={name} />
      <div>
        <p>{name}</p>
        <p><b>{price} KGS</b></p>
      </div>
      <button onClick={handler}>Добавить в корзину</button>
    </div>
  );
};

export default Product;
