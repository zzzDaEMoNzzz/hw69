import React from 'react';
import './OrderItem.css';

const OrderItem = ({name, count, price, handler}) => {
  return (
    <div className="OrderItem" onClick={handler}>
      <span>{name}</span>
      <span className="OrderItem-count">x{count}</span>
      <span>{price * count}</span>
    </div>
  );
};

export default OrderItem;
