import {CHECKOUT_HIDE, CHECKOUT_SHOW, ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "../actions/actionTypes";

const initialState = {
  show: false,
  loading: false,
};

const checkoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECKOUT_SHOW:
      return {...state, show: true};
    case CHECKOUT_HIDE:
      return {...state, show: false};
    case ORDER_REQUEST:
      return {...state, loading: true};
    case ORDER_SUCCESS:
      return {...state, loading: false, show: false};
    case ORDER_FAILURE:
      return {...state, loading: false};
    default:
      return state;
  }
};

export default checkoutReducer;