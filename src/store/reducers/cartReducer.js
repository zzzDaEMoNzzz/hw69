import {
  ADD_TO_CART, CLEAR_CART,
  REMOVE_FROM_CART
} from "../actions/actionTypes";

const initialState = {
  products: {},
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        products: {
          ...state.products,
          [action.id]: state.products[action.id] + 1 || 1
        }
      };
    case REMOVE_FROM_CART:
      return {
        ...state,
        products: {
          ...state.products,
          [action.id]: 0
        }
      };
    case CLEAR_CART:
      return {
        ...state,
        products: {}
      };
    default:
      return state;
  }
};

export default cartReducer;