import {
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_REQUEST,
  GET_PRODUCTS_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  products: [],
  loading: false,
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS_REQUEST:
      return {...state, loading: true};
    case GET_PRODUCTS_SUCCESS:
      return {...state, loading: false, products: action.products};
    case GET_PRODUCTS_ERROR:
      return {...state, loading: false};
    default:
      return state;
  }
};

export default productsReducer;