import axios from 'axios';
import {GET_PRODUCTS_ERROR, GET_PRODUCTS_REQUEST, GET_PRODUCTS_SUCCESS} from "./actionTypes";

export const getProductsRequest = () => ({type: GET_PRODUCTS_REQUEST});
export const getProductsSuccess = products => ({type: GET_PRODUCTS_SUCCESS, products});
export const getProductsError = error => ({type: GET_PRODUCTS_ERROR, error});

export const getProducts = () => {
  return dispatch => {
    dispatch(getProductsRequest());

    axios.get('products.json').then(
      response => {
        const products = Object.keys(response.data).map(id => {
          return {
            id,
            name: response.data[id].name,
            price: response.data[id].price,
            image: response.data[id].image,
            count: 0,
          };
        });

        dispatch(getProductsSuccess(products));
      },
      error => dispatch(getProductsError(error))
    );
  };
};