import axios from 'axios';
import {CHECKOUT_HIDE, CHECKOUT_SHOW, ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./actionTypes";
import {clearCart} from "./cart";

export const checkoutShow = () => ({type: CHECKOUT_SHOW});
export const checkoutHide = () => ({type: CHECKOUT_HIDE});

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = () => ({type: ORDER_SUCCESS});
export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const createOrder = (event, orderData) => {
  event.preventDefault();

  return dispatch => {
    dispatch(orderRequest());

    axios.post('orders.json', orderData).then(
      response => {
        dispatch(orderSuccess());
        dispatch(clearCart());
      },
      error => dispatch(orderFailure(error))
    );
  }
};