import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {Provider} from "react-redux";
import productsReducer from "./store/reducers/productsReducer";
import cartReducer from "./store/reducers/cartReducer";
import App from './App';
import './index.css';

import axios from 'axios';
import checkoutReducer from "./store/reducers/checkoutReducer";
axios.defaults.baseURL = 'https://kurlov-hw69.firebaseio.com/';

const rootReducer = combineReducers({
  pr: productsReducer,
  cart: cartReducer,
  checkout: checkoutReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

const app = (
  <Provider store={store}>
    <App/>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));