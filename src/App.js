import React, { Component } from 'react';
import './App.css';
import Products from "./containers/Products/Products";
import Cart from "./containers/Cart/Cart";
import Modal from "./components/UI/Modal/Modal";
import Checkout from "./containers/Checkout/Checkout";
import {connect} from "react-redux";
import {checkoutHide} from "./store/actions/checkout";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Modal show={this.props.show} close={this.props.checkoutHide}>
          <Checkout/>
        </Modal>
        <div className="App-body">
          <Products/>
          <Cart/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  show: state.checkout.show
});

const mapDispatchToProps = dispatch => ({
  checkoutHide: () => dispatch(checkoutHide())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);